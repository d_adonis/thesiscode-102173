# Thesiscode 102173


This repo contains the code for the MsC thesis "Detection of cyber security attacks in 5G Networks"

## Network trace-based approach

The included files are:

- extractor.py
- flow.py
- ifclassifier.py
- packetgrouper.py

Other relevant links:

- [5GAD Dataset](https://github.com/IdahoLabResearch/5GAD)
- [5GNIDD Dataset](https://ieee-dataport.org/documents/5g-nidd-comprehensive-network-intrusion-detection-dataset-generated-over-5g-wireless)
- [DeepPacket](https://github.com/munhouiani/Deep-Packet)
- [Suricata](https://suricata.io/)


## Log-based approach

- clustering.py
- [DeepLog](https://github.com/nailo2c/deeplog)
- [Drain3](https://github.com/logpai/Drain3)
